package main

import (
	"fmt"
	"math"
)

func main() {
	var amount, rate, period int
	fmt.Scanf("%d %d %d", &amount, &rate, &period)

	var r float64 = float64(float64(rate) / float64(100))
	var p float64 = float64(float64(period) * float64(12))
	var a float64 = float64(amount)
	power := math.Pow((1 + r/12), 12)
	var count int

	if period == 1 {
		count = 11
		principal := a*(r/p)*power/(power-1) - a*(r/p)
		payment := principal + (a*r)/12
		interest := payment - principal
		balance := a - principal

		fmt.Printf("%.2f %.2f %.2f %.2f\n", principal, payment, interest, balance)
		for i := 1; i <= count; i++ {
			principal = principal * (1 + (r / 12))
			interest = payment - principal
			balance = balance - principal

			fmt.Printf("%.2f %.2f %.2f %.2f\n", principal, payment, interest, balance)
		}
	} // else if period == 2 {
	// 	count = 23

	// 	principal := a*(r/24)*power/(power-1) - a*(r/24)
	// 	payment := principal + (a*r)/12
	// 	interest := payment - principal
	// 	balance := a - principal

	// 	fmt.Printf("%.2f %.2f %.2f %.2f\n", principal, payment, interest, balance)
	// 	for i := 1; i <= count; i++ {
	// 		principal = principal * (1 + (r / 12))
	// 		interest = payment - principal
	// 		balance = balance - principal

	// 		fmt.Printf("%.2f %.2f %.2f %.2f\n", principal, payment, interest, balance)
	// 	}
	// }
}
